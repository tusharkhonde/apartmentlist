# ApartmentList Coding Challenge

    Steps to run 
        
        * git clone https://tusharkhonde@bitbucket.org/tusharkhonde/apartmentlist.git
        * mvn compile
        * mvn install -DskipTests
        * mvn tests
        
        To run for custom dictionary - 
            
            - java -cp target/com.company-1.0-SNAPSHOT.jar apartmentlist.ApartmentList arg_1 arg_2
            - arg_1 -> input word 
            - arg_2 -> file path (dictionary)