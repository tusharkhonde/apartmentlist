import apartmentlist.SocialNetwork;
import org.junit.Assert;
import org.junit.Test;

import java.net.URL;

public class SocialNetworkTest {

    @Test
    public void testFullGraphSize() throws Exception {

        URL url = this.getClass().getResource("/" + "dictionary.txt");

        Assert.assertEquals(51710, SocialNetwork.socialNetwork("LISTY", url.getFile()));
    }

    @Test
    public void testHalfGraphSize() throws Exception {

        URL url = this.getClass().getResource("/" + "half_dictionary.txt");

        Assert.assertEquals(22741, SocialNetwork.socialNetwork("LISTY", url.getFile()));
    }

    @Test
    public void testQuarterGraphSize() throws Exception {

        URL url = this.getClass().getResource("/" + "quarter_dictionary.txt");

        Assert.assertEquals(11008, SocialNetwork.socialNetwork("LISTY", url.getFile()));
    }

    @Test
    public void testEighthGraphSize() throws Exception {

        URL url = this.getClass().getResource("/" + "eighth_dictionary.txt");

        Assert.assertEquals(5132, SocialNetwork.socialNetwork("LISTY", url.getFile()));
    }

    @Test
    public void testVerySmallGraph() throws Exception {

        URL url = this.getClass().getResource("/" + "very_small_test_dictionary.txt");

        Assert.assertEquals(5, SocialNetwork.socialNetwork("LISTY", url.getFile()));
    }




}
