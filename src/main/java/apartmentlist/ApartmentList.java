package apartmentlist;


import java.io.File;

public class ApartmentList {

    public static void main(String[] args) throws Exception {


        if (args.length >= 2) {

            String value = args[0];
            value = value.toUpperCase();

            String fileName = args[1];

            File file = new File(fileName);

            if (!file.exists()){
                System.out.println("File not Found, Enter correct file path");
                System.exit(0);
            }

            long start = System.currentTimeMillis() / 1000;

            int result = SocialNetwork.socialNetwork(value,fileName);

            System.out.println(result);

            long timeLength = (System.currentTimeMillis() / 1000) - start;

            System.out.println("Social Network Size for " + value + ": " + result + " (completed in " + timeLength + " sec)");
        }

        else {
            System.out.println("Please enter a input arguments ...");
        }
    }

}
