package apartmentlist;

class Helper {
    /*
        Checks if two strings are at edit distance of 1
     */
    static boolean isEditDistanceOne(String stringOne, String stringTwo) {

        int oneLength = stringOne.length();
        int twoLength = stringTwo.length();


        int countOne = 0;
        int countTwo = 0;
        int differenceCounter = 0;


        while (countOne < oneLength && countTwo < twoLength) {

            /*
            * Check for character at respective positions in first string and second string
            * */

            if (stringOne.charAt(countOne) != stringTwo.charAt(countTwo)) {
                differenceCounter = differenceCounter + 1;

                /*
                  If the differences is greater than one, the two strings can't have an edit distance of 1
                 */
                if (differenceCounter > 1) {
                    return false;
                }

                /*
                  If the first string is longer than the second, increment the counter for the first string
                 */
                if (oneLength > twoLength) {
                    countOne = countOne + 1;
                }

                /*
                  If the second string is longer than the first, increment the counter for the second string
                 */
                else if (twoLength > oneLength) {
                    countTwo = countTwo +1;
                }

                /*
                     If both are of same length, increment both counters
                 */
                else {
                    countOne = countOne + 1;
                    countTwo = countTwo + 1;
                }
            }

            else {
                countOne = countOne + 1;
                countTwo = countTwo + 1;
            }
        }

        /*
            Continue looping until both strings are completely traversed.
         * */
        while (countOne < oneLength || countTwo < twoLength) {
            if (countOne < oneLength) {
                differenceCounter++;
                countOne++;
            }
            else {
                differenceCounter++;
                countTwo++;
            }
        }


        return differenceCounter == 1;


    }

    /*
     * Check if input string already visited
    */
    static boolean alreadyUsed(String string, String[] alreadyUsedList, int manyItems) {

        boolean alreadyUsed = false;

        for (int i = 0; i < manyItems; i++) {
            String wordToCompare = alreadyUsedList[i];
            if (string.equals(wordToCompare)) {
                alreadyUsed = true;
            }
        }

        return alreadyUsed;

    }
}
