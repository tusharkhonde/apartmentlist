package apartmentlist;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Stack;

import static apartmentlist.Helper.alreadyUsed;
import static apartmentlist.Helper.isEditDistanceOne;


public class SocialNetwork {

    /* Calculates the social network size for a input word by
     * Find all friends that are withing the edit distance of the input word
     * Push all of its friends that have not been visited to a stack of words to be checked.
     * Until stack is not empty, repeat with the top word in the stack */

    public static int socialNetwork(String string, String fileName) {

        File file = new File(fileName);

        String[][] a = new String[15][100000];

            /*
             * "size" array will help to traverse the number of friends for each word
             * each word will compared to a smaller subset of the whole dictionary
             */

        int[] size = new int[16];

        try {
            Scanner scanner = new Scanner(file);

                /* Add the input word to the appropriate spot in the matrix
                 * For example,  the eighth word with a length of 10, will go into the spot [10][7]
                 *   10 => length of the word
                 *   7 => 7th word of that length = 10
                 **/
            while (scanner.hasNextLine()) {
                String expression = scanner.nextLine();
                int length = expression.length();
                int location = size[length];
                a[length - 1][location] = expression;
                size[length] = size[length] + 1;

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


        int counter = 0;

        String[] alreadyUsedList = new String[100000];

        int manyItems = 0;

        Stack<String> wordsToCheckStack = new Stack<String>();

        wordsToCheckStack.push(string);

        while (!wordsToCheckStack.isEmpty()) {

            String wordToCheck = wordsToCheckStack.pop();

            int length = wordToCheck.length();

            /* Traverse through a small subset of the entire dictionary.
            * For example, word of length 10
            * Words with length 9, 10, and 11 as they are at edit distance of one.
            * It helps to visit just small portion of the dictionary instead of the whole collection
            * */

            for (int i = length - 2; i <= length; i++) {

                int wordSize = size[i+1];

                for (int k = 0; k < wordSize; k++) {

                    String wordToCompare = a[i][k];

                    if (isEditDistanceOne(wordToCheck, wordToCompare) && !alreadyUsed(wordToCompare, alreadyUsedList, manyItems)) {

                        alreadyUsedList[manyItems] = wordToCompare;

                        manyItems++;

                        wordsToCheckStack.push(wordToCompare);

                        counter++;

                    }
                }
            }

        }
        return counter;
    }

}
